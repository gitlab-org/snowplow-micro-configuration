# Snowplow Micro configuration

[Snowplow Micro](https://docs.snowplow.io/docs/testing-debugging/snowplow-micro/what-is-micro/)
is a lightweight version of the Snowplow pipeline that can be used for debugging
and testing. This project provides a convenient way to run Snowplow Micro by using a container.

This project:

- Is Docker-based and easy to setup.
- Enables you to debug how custom contexts and self-describing events are resolved.
- Enables you use the [Snowplow Analytics Debugger](https://chrome.google.com/webstore/detail/snowplow-analytics-debugg/jbnlcgeengmijcghameodeaenefieedm) Chrome extension to make
  development easier.
- Has configuration that points to GitLab [Iglu resolver registry](https://docs.snowplow.io/docs/pipeline-components-and-applications/iglu/iglu-resolver/).

For questions regarding the project contact the [maintainers](https://handbook.gitlab.com/handbook/engineering/projects/#snowplow-micro-configuration).

## Get started

1. Clone this repo.
1. Run `./snowplow-micro.sh` to start Snowplow Micro.

Run `./snowplow-micro.sh -p <port>` to start Snowplow Micro on a port other than `9090`.

## View events

In your browser you can view:

- [Debug events](http://localhost:9090/micro).
- [Good events](http://localhost:9090/micro/good).
- [Bad events](http://localhost:9090/micro/bad). Bad events are Base64 encoded and can be decoded using `base64 -d`.
- [All events](http://localhost:9090/micro/all).

## Reset queues

To reset the queues, go to <http://localhost:9090/micro/reset>.

## Configure for GitLab by using GDK

Configuring your local development instance in GDK is difficult. For help, contact the [Analytics Instrumentation](https://about.gitlab.com/handbook/engineering/development/analytics/analytics-instrumentation/) group.

## References

- [1] https://github.com/snowplow-incubator/snowplow-micro
- [2] https://snowplowanalytics.com/blog/2019/07/17/introducing-snowplow-micro/
