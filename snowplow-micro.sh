#!/usr/bin/env bash

set -eu

# Default port
PORT=9090

while getopts "p:" opt; do
  case $opt in
    p) PORT=$OPTARG ;;
    *) echo "Usage: $0 [-p port]"
       exit 1 ;;
  esac
done

echo "Starting Snowplow Micro on port $PORT."
echo "The following endpoints are available:"
echo "Summary: http://localhost:$PORT/micro/all"
echo "Events:  http://localhost:$PORT/micro/good"
echo "Errors:  http://localhost:$PORT/micro/bad"
echo "Reset:   http://localhost:$PORT/micro/reset"
echo

docker run --mount type=bind,source="$(pwd)/config",destination=/config \
  -p "$PORT":9090 snowplow/snowplow-micro:latest \
  --collector-config /config/micro.conf \
  --iglu /config/iglu.json
